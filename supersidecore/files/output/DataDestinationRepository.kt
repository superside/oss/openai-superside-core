package com.superads.api.dataDestination

import com.superside.core.jooq.repository.JooqRepository
import com.superside.core.model.Id
import com.superside.superads.domain.jooq.Tables.DATA_DESTINATION
import com.superside.superads.domain.jooq.tables.records.DataDestinationRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Service
import com.superside.superads.domain.jooq.tables.DataDestination as DataDestinationTable

@Service
class DataDestinationRepository(
    dslContext: DSLContext,
) : JooqRepository<DataDestinationModel, DataDestinationRecord, DataDestinationTable>(
    dslContext = dslContext,
    table = DATA_DESTINATION,
    idField = DATA_DESTINATION.UUID,
    updatedAtField = DATA_DESTINATION.UPDATED_AT,
    deletedAtField = DATA_DESTINATION.DELETED_AT,
) {
    override fun toModel(record: DataDestinationRecord): DataDestinationModel {

        return DataDestinationModel(
            id = Id.fromUUID(record.uuid),
            createdAt = record.createdAt!!.toInstant(),
            updatedAt = record.updatedAt!!.toInstant(),
            deletedAt = record.deletedAt?.toInstant(),
            type = DataDestinationType.fromName(record.type)!!,
            workspaceId = Id.fromUUID(record.workspaceUuid),
            airbyteDestinationId = record.airbyteDestinationUuid
        )
    }

    override fun toRecord(model: DataDestinationModel): DataDestinationRecord {
        return DataDestinationRecord().apply {
            uuid = model.id.value
            createdAt = model.createdAt.toLocalDateTime(DATA_DESTINATION.CREATED_AT)
            updatedAt = model.updatedAt.toLocalDateTime(DATA_DESTINATION.CREATED_AT)
            deletedAt = model.deletedAt?.toLocalDateTime(DATA_DESTINATION.CREATED_AT)
            type = model.type.name
            workspaceUuid = model.workspaceId.value
            airbyteDestinationUuid = model.airbyteDestinationId
        }
    }
}
