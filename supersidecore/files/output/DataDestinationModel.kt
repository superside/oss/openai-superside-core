package com.superads.api.dataDestination

import com.superads.api.dataDestination.dto.CreateDataDestination
import com.superads.api.workspace.Workspace
import com.superside.core.model.BaseModel
import com.superside.core.model.Id
import java.time.Instant
import java.util.UUID

data class DataDestinationModel (
    override val id: Id<DataDestinationModel>,
    override val createdAt: Instant,
    override val updatedAt: Instant,
    override val deletedAt: Instant? = null,
    val workspaceId: Id<Workspace>,
    val airbyteDestinationId: UUID? = null,
    val type: DataDestinationType
): BaseModel<DataDestinationModel>() {
    companion object {
        fun create(req: CreateDataDestination): DataDestinationModel {
            val now = Instant.now()
            return DataDestinationModel(
                id = req.id ?: Id.randomId(),
                createdAt = now,
                updatedAt = now,
                type = req.type,
                workspaceId = req.workspaceId,
                airbyteDestinationId = req.airbyteDestinationId,
            )
        }
    }
}

