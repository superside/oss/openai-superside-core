package com.superads.api.workspace

import com.superside.core.model.BaseModel
import com.superside.core.model.Id
import java.time.Instant
import java.time.Instant.now
import java.util.UUID

data class WorkspaceModel internal constructor(
    override val id: Id<WorkspaceModel>,
    override val createdAt: Instant,
    override val updatedAt: Instant,
    override val deletedAt: Instant? = null,
    val name: String,
    val airbyteWorkspaceId: UUID? = null
): BaseModel<WorkspaceModel>(){

    fun withAirbyteWorkspaceId(airbyteWorkspaceId: UUID): WorkspaceModel {
        return update(
            isModelSame = { it.airbyteWorkspaceId == airbyteWorkspaceId },
            updater = { copy(updatedAt = now(), airbyteWorkspaceId = airbyteWorkspaceId) },
        )
    }

    fun delete(): WorkspaceModel {
        return update(
            isModelSame = { it.deletedAt != null },
            updater = {
                val now = now()
                copy(updatedAt = now, deletedAt = now)
            },
        )
    }

    companion object {
        fun create(req: CreateWorkspaceRequest): WorkspaceModel {
            val now =  Instant.now()
            return WorkspaceModel(
                id = req.id ?: Id.randomId(),
                createdAt = now,
                updatedAt = now,
                name = req.name,
                airbyteWorkspaceId = req.airbyteWorkspaceId
            )
        }


    }
}

data class CreateWorkspaceRequest (
    val name: String,
    val id: Id<WorkspaceModel>? = null,
    val airbyteWorkspaceId: UUID? = null
)
