package com.superads.api.adsAccount

import com.superads.api.workspace.WorkspaceModel
import com.superside.core.model.BaseModel
import com.superside.core.model.Id
import java.time.Instant
import java.util.UUID

data class AdsAccountModel internal constructor(
    override val id: Id<AdsAccountModel>,
    override val createdAt: Instant,
    override val updatedAt: Instant,
    override val deletedAt: Instant? = null,
    val type: AdsAccountType,
    val adsAccountId: String,
    val adsAccountName: String,
    val airbyteSourceId: UUID? = null,
    val workspaceId: Id<WorkspaceModel>,
    val accessToken: String
) : BaseModel<AdsAccountModel>() {

    fun delete(): AdsAccountModel {
        return update(
            isModelSame = { it.deletedAt != null },
            updater = {
                val now = Instant.now()
                copy(updatedAt = now, deletedAt = now)
            },
        )
    }
    companion object {
        fun create(req: CreateAdsAccountRequest): AdsAccountModel {
            val now = Instant.now()
            return AdsAccountModel(
                id = req.id ?: Id.randomId(),
                createdAt = now,
                updatedAt = now,
                type = req.type,
                adsAccountId = req.adsAccountId,
                adsAccountName = req.adsAccountName,
                airbyteSourceId = req.airbyteSourceId,
                workspaceId = req.workspaceId,
                accessToken = req.accessToken
            )
        }
    }
}


data class CreateAdsAccountRequest(
    val id: Id<AdsAccountModel>? = null,
    val type: AdsAccountType,
    val adsAccountId: String,
    val adsAccountName: String,
    val airbyteSourceId: UUID,
    val workspaceId: Id<WorkspaceModel>,
    val accessToken: String
)
