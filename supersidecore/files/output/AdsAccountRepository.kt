package com.superads.api.adsAccount

import com.superside.core.jooq.repository.JooqRepository
import com.superside.core.model.Id
import com.superside.superads.domain.jooq.Tables.ADS_ACCOUNT
import com.superside.superads.domain.jooq.tables.records.AdsAccountRecord
import org.jooq.DSLContext
import org.springframework.stereotype.Service
import com.superside.superads.domain.jooq.tables.AdsAccount as AdsAccountTable

@Service
class AdsAccountRepository(
    dslContext: DSLContext,
) : JooqRepository<AdsAccount, AdsAccountRecord, AdsAccountTable>(
    dslContext = dslContext,
    table = ADS_ACCOUNT,
    idField = ADS_ACCOUNT.UUID,
    updatedAtField = ADS_ACCOUNT.UPDATED_AT,
    deletedAtField = ADS_ACCOUNT.DELETED_AT,
) {
    override fun toModel(record: AdsAccountRecord): AdsAccount {
        return AdsAccount(
            id = Id.fromUUID(record.uuid),
            createdAt = record.createdAt!!.toInstant(),
            updatedAt = record.updatedAt!!.toInstant(),
            deletedAt = record.deletedAt?.toInstant(),
            type = AdsAccountType.valueOf(record.type),
            adsAccountId = record.adsAccountId,
            adsAccountName = record.adsAccountName,
            airbyteSourceId = record.airbyteSourceUuid,
            workspaceId = Id.fromUUID(record.workspaceUuid),
            accessToken = record.accessToken
        )
    }

    override fun toRecord(model: AdsAccount): AdsAccountRecord {
        return AdsAccountRecord().apply {
            uuid = model.id.value
            createdAt = model.createdAt.toLocalDateTime(ADS_ACCOUNT.CREATED_AT)
            updatedAt = model.updatedAt.toLocalDateTime(ADS_ACCOUNT.CREATED_AT)
            deletedAt = model.deletedAt?.toLocalDateTime(ADS_ACCOUNT.CREATED_AT)
            type = model.type.name
            adsAccountId = model.adsAccountId
            adsAccountName = model.adsAccountName
            airbyteSourceUuid = model.airbyteSourceId
            workspaceUuid = model.workspaceId.value
            accessToken = model.accessToken
        }
    }
}
