package com.superads.api.workspace

import com.superside.core.jooq.repository.JooqRepository
import com.superside.core.model.Id
import com.superside.superads.domain.jooq.Tables.WORKSPACE
import com.superside.superads.domain.jooq.tables.records.WorkspaceRecord
import com.superside.superads.domain.jooq.tables.Workspace as WorkspaceTable
import org.jooq.DSLContext
import org.springframework.stereotype.Service


@Service
class WorkspaceRepository(
    dslContext: DSLContext,
) : JooqRepository<Workspace, WorkspaceRecord, WorkspaceTable>(
    dslContext = dslContext,
    table = WORKSPACE,
    idField = WORKSPACE.UUID,
    updatedAtField = WORKSPACE.UPDATED_AT,
    deletedAtField = WORKSPACE.DELETED_AT,
) {
    override fun toModel(record: WorkspaceRecord): Workspace {
        return Workspace(
            id = Id.fromUUID(record.uuid),
            name = record.name,
            createdAt = record.createdAt!!.toInstant(),
            updatedAt = record.updatedAt!!.toInstant(),
            deletedAt = record.deletedAt?.toInstant(),
            airbyteWorkspaceId = record.airbyteWorkspaceUuid
        )
    }

    override fun toRecord(model: Workspace): WorkspaceRecord {
        return WorkspaceRecord().apply {
            uuid = model.id.value
            name = model.name
            createdAt = model.createdAt.toLocalDateTime(WORKSPACE.CREATED_AT)
            updatedAt = model.updatedAt.toLocalDateTime(WORKSPACE.UPDATED_AT)
            deletedAt = model.deletedAt?.toLocalDateTime(WORKSPACE.DELETED_AT)
            airbyteWorkspaceUuid = model.airbyteWorkspaceId
        }
    }
    fun findAll(): Collection<Workspace> {
        return execute(
            query = {
                selectFrom(table)
            },
            resultFetcher = {
                fetch { toModel(it) }
            }

        )
    }
}
