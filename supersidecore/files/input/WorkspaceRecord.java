
package com.superside.superads.domain.jooq.tables;


import java.time.LocalDateTime;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;



@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class WorkspaceRecord extends UpdatableRecordImpl<WorkspaceRecord> implements Record7<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, UUID, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>workspace.id</code>.
     */
    public WorkspaceRecord setId(@Nullable Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>workspace.id</code>.
     */
    @Nullable
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>workspace.uuid</code>.
     */
    public WorkspaceRecord setUuid(@NotNull UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>workspace.uuid</code>.
     */
    @NotNull
    public UUID getUuid() {
        return (UUID) get(1);
    }

    /**
     * Setter for <code>workspace.created_at</code>.
     */
    public WorkspaceRecord setCreatedAt(@Nullable LocalDateTime value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>workspace.created_at</code>.
     */
    @Nullable
    public LocalDateTime getCreatedAt() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>workspace.updated_at</code>.
     */
    public WorkspaceRecord setUpdatedAt(@Nullable LocalDateTime value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>workspace.updated_at</code>.
     */
    @Nullable
    public LocalDateTime getUpdatedAt() {
        return (LocalDateTime) get(3);
    }

    /**
     * Setter for <code>workspace.deleted_at</code>.
     */
    public WorkspaceRecord setDeletedAt(@Nullable LocalDateTime value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>workspace.deleted_at</code>.
     */
    @Nullable
    public LocalDateTime getDeletedAt() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>workspace.airbyte_workspace_uuid</code>.
     */
    public WorkspaceRecord setAirbyteWorkspaceUuid(@Nullable UUID value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>workspace.airbyte_workspace_uuid</code>.
     */
    @Nullable
    public UUID getAirbyteWorkspaceUuid() {
        return (UUID) get(5);
    }

    /**
     * Setter for <code>workspace.name</code>.
     */
    public WorkspaceRecord setName(@NotNull String value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>workspace.name</code>.
     */
    @NotNull
    public String getName() {
        return (String) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Row7<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, UUID, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    @NotNull
    public Row7<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, UUID, String> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    @NotNull
    public Field<Long> field1() {
        return Workspace.WORKSPACE.ID;
    }

    @Override
    @NotNull
    public Field<UUID> field2() {
        return Workspace.WORKSPACE.UUID;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field3() {
        return Workspace.WORKSPACE.CREATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field4() {
        return Workspace.WORKSPACE.UPDATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field5() {
        return Workspace.WORKSPACE.DELETED_AT;
    }

    @Override
    @NotNull
    public Field<UUID> field6() {
        return Workspace.WORKSPACE.AIRBYTE_WORKSPACE_UUID;
    }

    @Override
    @NotNull
    public Field<String> field7() {
        return Workspace.WORKSPACE.NAME;
    }

    @Override
    @Nullable
    public Long component1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID component2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime component3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component5() {
        return getDeletedAt();
    }

    @Override
    @Nullable
    public UUID component6() {
        return getAirbyteWorkspaceUuid();
    }

    @Override
    @NotNull
    public String component7() {
        return getName();
    }

    @Override
    @Nullable
    public Long value1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID value2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime value3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value5() {
        return getDeletedAt();
    }

    @Override
    @Nullable
    public UUID value6() {
        return getAirbyteWorkspaceUuid();
    }

    @Override
    @NotNull
    public String value7() {
        return getName();
    }

    @Override
    @NotNull
    public WorkspaceRecord value1(@Nullable Long value) {
        setId(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value2(@NotNull UUID value) {
        setUuid(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value3(@Nullable LocalDateTime value) {
        setCreatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value4(@Nullable LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value5(@Nullable LocalDateTime value) {
        setDeletedAt(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value6(@Nullable UUID value) {
        setAirbyteWorkspaceUuid(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord value7(@NotNull String value) {
        setName(value);
        return this;
    }

    @Override
    @NotNull
    public WorkspaceRecord values(@Nullable Long value1, @NotNull UUID value2, @Nullable LocalDateTime value3, @Nullable LocalDateTime value4, @Nullable LocalDateTime value5, @Nullable UUID value6, @NotNull String value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached WorkspaceRecord
     */
    public WorkspaceRecord() {
        super(Workspace.WORKSPACE);
    }

    /**
     * Create a detached, initialised WorkspaceRecord
     */
    public WorkspaceRecord(@Nullable Long id, @NotNull UUID uuid, @Nullable LocalDateTime createdAt, @Nullable LocalDateTime updatedAt, @Nullable LocalDateTime deletedAt, @Nullable UUID airbyteWorkspaceUuid, @NotNull String name) {
        super(Workspace.WORKSPACE);

        setId(id);
        setUuid(uuid);
        setCreatedAt(createdAt);
        setUpdatedAt(updatedAt);
        setDeletedAt(deletedAt);
        setAirbyteWorkspaceUuid(airbyteWorkspaceUuid);
        setName(name);
        resetChangedOnNotNull();
    }

    /**
     * Create a detached, initialised WorkspaceRecord
     */
    public WorkspaceRecord(com.superside.superads.domain.jooq.tables.pojos.Workspace value) {
        super(Workspace.WORKSPACE);

        if (value != null) {
            setId(value.getId());
            setUuid(value.getUuid());
            setCreatedAt(value.getCreatedAt());
            setUpdatedAt(value.getUpdatedAt());
            setDeletedAt(value.getDeletedAt());
            setAirbyteWorkspaceUuid(value.getAirbyteWorkspaceUuid());
            setName(value.getName());
            resetChangedOnNotNull();
        }
    }
}
