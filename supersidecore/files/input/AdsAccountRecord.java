
package com.superside.superads.domain.jooq.tables;


import java.time.LocalDateTime;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record11;
import org.jooq.Row11;
import org.jooq.impl.UpdatableRecordImpl;

@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AdsAccountRecord extends UpdatableRecordImpl<AdsAccountRecord> implements Record11<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, String, String, UUID, UUID, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>ads_account.id</code>.
     */
    public AdsAccountRecord setId(@Nullable Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.id</code>.
     */
    @Nullable
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>ads_account.uuid</code>.
     */
    public AdsAccountRecord setUuid(@NotNull UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.uuid</code>.
     */
    @NotNull
    public UUID getUuid() {
        return (UUID) get(1);
    }

    /**
     * Setter for <code>ads_account.created_at</code>.
     */
    public AdsAccountRecord setCreatedAt(@Nullable LocalDateTime value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.created_at</code>.
     */
    @Nullable
    public LocalDateTime getCreatedAt() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>ads_account.updated_at</code>.
     */
    public AdsAccountRecord setUpdatedAt(@Nullable LocalDateTime value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.updated_at</code>.
     */
    @Nullable
    public LocalDateTime getUpdatedAt() {
        return (LocalDateTime) get(3);
    }

    /**
     * Setter for <code>ads_account.deleted_at</code>.
     */
    public AdsAccountRecord setDeletedAt(@Nullable LocalDateTime value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.deleted_at</code>.
     */
    @Nullable
    public LocalDateTime getDeletedAt() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>ads_account.type</code>.
     */
    public AdsAccountRecord setType(@NotNull String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.type</code>.
     */
    @NotNull
    public String getType() {
        return (String) get(5);
    }

    /**
     * Setter for <code>ads_account.ads_account_id</code>.
     */
    public AdsAccountRecord setAdsAccountId(@NotNull String value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.ads_account_id</code>.
     */
    @NotNull
    public String getAdsAccountId() {
        return (String) get(6);
    }

    /**
     * Setter for <code>ads_account.ads_account_name</code>.
     */
    public AdsAccountRecord setAdsAccountName(@NotNull String value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.ads_account_name</code>.
     */
    @NotNull
    public String getAdsAccountName() {
        return (String) get(7);
    }

    /**
     * Setter for <code>ads_account.airbyte_source_uuid</code>.
     */
    public AdsAccountRecord setAirbyteSourceUuid(@Nullable UUID value) {
        set(8, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.airbyte_source_uuid</code>.
     */
    @Nullable
    public UUID getAirbyteSourceUuid() {
        return (UUID) get(8);
    }

    /**
     * Setter for <code>ads_account.workspace_uuid</code>.
     */
    public AdsAccountRecord setWorkspaceUuid(@NotNull UUID value) {
        set(9, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.workspace_uuid</code>.
     */
    @NotNull
    public UUID getWorkspaceUuid() {
        return (UUID) get(9);
    }

    /**
     * Setter for <code>ads_account.access_token</code>.
     */
    public AdsAccountRecord setAccessToken(@NotNull String value) {
        set(10, value);
        return this;
    }

    /**
     * Getter for <code>ads_account.access_token</code>.
     */
    @NotNull
    public String getAccessToken() {
        return (String) get(10);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record11 type implementation
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Row11<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, String, String, UUID, UUID, String> fieldsRow() {
        return (Row11) super.fieldsRow();
    }

    @Override
    @NotNull
    public Row11<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, String, String, UUID, UUID, String> valuesRow() {
        return (Row11) super.valuesRow();
    }

    @Override
    @NotNull
    public Field<Long> field1() {
        return AdsAccount.ADS_ACCOUNT.ID;
    }

    @Override
    @NotNull
    public Field<UUID> field2() {
        return AdsAccount.ADS_ACCOUNT.UUID;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field3() {
        return AdsAccount.ADS_ACCOUNT.CREATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field4() {
        return AdsAccount.ADS_ACCOUNT.UPDATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field5() {
        return AdsAccount.ADS_ACCOUNT.DELETED_AT;
    }

    @Override
    @NotNull
    public Field<String> field6() {
        return AdsAccount.ADS_ACCOUNT.TYPE;
    }

    @Override
    @NotNull
    public Field<String> field7() {
        return AdsAccount.ADS_ACCOUNT.ADS_ACCOUNT_ID;
    }

    @Override
    @NotNull
    public Field<String> field8() {
        return AdsAccount.ADS_ACCOUNT.ADS_ACCOUNT_NAME;
    }

    @Override
    @NotNull
    public Field<UUID> field9() {
        return AdsAccount.ADS_ACCOUNT.AIRBYTE_SOURCE_UUID;
    }

    @Override
    @NotNull
    public Field<UUID> field10() {
        return AdsAccount.ADS_ACCOUNT.WORKSPACE_UUID;
    }

    @Override
    @NotNull
    public Field<String> field11() {
        return AdsAccount.ADS_ACCOUNT.ACCESS_TOKEN;
    }

    @Override
    @Nullable
    public Long component1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID component2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime component3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component5() {
        return getDeletedAt();
    }

    @Override
    @NotNull
    public String component6() {
        return getType();
    }

    @Override
    @NotNull
    public String component7() {
        return getAdsAccountId();
    }

    @Override
    @NotNull
    public String component8() {
        return getAdsAccountName();
    }

    @Override
    @Nullable
    public UUID component9() {
        return getAirbyteSourceUuid();
    }

    @Override
    @NotNull
    public UUID component10() {
        return getWorkspaceUuid();
    }

    @Override
    @NotNull
    public String component11() {
        return getAccessToken();
    }

    @Override
    @Nullable
    public Long value1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID value2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime value3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value5() {
        return getDeletedAt();
    }

    @Override
    @NotNull
    public String value6() {
        return getType();
    }

    @Override
    @NotNull
    public String value7() {
        return getAdsAccountId();
    }

    @Override
    @NotNull
    public String value8() {
        return getAdsAccountName();
    }

    @Override
    @Nullable
    public UUID value9() {
        return getAirbyteSourceUuid();
    }

    @Override
    @NotNull
    public UUID value10() {
        return getWorkspaceUuid();
    }

    @Override
    @NotNull
    public String value11() {
        return getAccessToken();
    }

    @Override
    @NotNull
    public AdsAccountRecord value1(@Nullable Long value) {
        setId(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value2(@NotNull UUID value) {
        setUuid(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value3(@Nullable LocalDateTime value) {
        setCreatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value4(@Nullable LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value5(@Nullable LocalDateTime value) {
        setDeletedAt(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value6(@NotNull String value) {
        setType(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value7(@NotNull String value) {
        setAdsAccountId(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value8(@NotNull String value) {
        setAdsAccountName(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value9(@Nullable UUID value) {
        setAirbyteSourceUuid(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value10(@NotNull UUID value) {
        setWorkspaceUuid(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord value11(@NotNull String value) {
        setAccessToken(value);
        return this;
    }

    @Override
    @NotNull
    public AdsAccountRecord values(@Nullable Long value1, @NotNull UUID value2, @Nullable LocalDateTime value3, @Nullable LocalDateTime value4, @Nullable LocalDateTime value5, @NotNull String value6, @NotNull String value7, @NotNull String value8, @Nullable UUID value9, @NotNull UUID value10, @NotNull String value11) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AdsAccountRecord
     */
    public AdsAccountRecord() {
        super(AdsAccount.ADS_ACCOUNT);
    }

    /**
     * Create a detached, initialised AdsAccountRecord
     */
    public AdsAccountRecord(@Nullable Long id, @NotNull UUID uuid, @Nullable LocalDateTime createdAt, @Nullable LocalDateTime updatedAt, @Nullable LocalDateTime deletedAt, @NotNull String type, @NotNull String adsAccountId, @NotNull String adsAccountName, @Nullable UUID airbyteSourceUuid, @NotNull UUID workspaceUuid, @NotNull String accessToken) {
        super(AdsAccount.ADS_ACCOUNT);

        setId(id);
        setUuid(uuid);
        setCreatedAt(createdAt);
        setUpdatedAt(updatedAt);
        setDeletedAt(deletedAt);
        setType(type);
        setAdsAccountId(adsAccountId);
        setAdsAccountName(adsAccountName);
        setAirbyteSourceUuid(airbyteSourceUuid);
        setWorkspaceUuid(workspaceUuid);
        setAccessToken(accessToken);
        resetChangedOnNotNull();
    }

    /**
     * Create a detached, initialised AdsAccountRecord
     */
    public AdsAccountRecord(AdsAccount value) {
        super(AdsAccount.ADS_ACCOUNT);

        if (value != null) {
            setId(value.getId());
            setUuid(value.getUuid());
            setCreatedAt(value.getCreatedAt());
            setUpdatedAt(value.getUpdatedAt());
            setDeletedAt(value.getDeletedAt());
            setType(value.getType());
            setAdsAccountId(value.getAdsAccountId());
            setAdsAccountName(value.getAdsAccountName());
            setAirbyteSourceUuid(value.getAirbyteSourceUuid());
            setWorkspaceUuid(value.getWorkspaceUuid());
            setAccessToken(value.getAccessToken());
            resetChangedOnNotNull();
        }
    }
}
