
package com.superside.superads.domain.jooq.tables;


import com.superside.superads.domain.jooq.DefaultSchema;
import com.superside.superads.domain.jooq.Keys;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function7;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row7;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;



@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Workspace extends TableImpl<WorkspaceRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>workspace</code>
     */
    public static final Workspace WORKSPACE = new Workspace();

    /**
     * The class holding records for this type
     */
    @Override
    @NotNull
    public Class<WorkspaceRecord> getRecordType() {
        return WorkspaceRecord.class;
    }

    /**
     * The column <code>workspace.id</code>.
     */
    public final TableField<WorkspaceRecord, Long> ID = createField(DSL.name("id"), SQLDataType.BIGINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>workspace.uuid</code>.
     */
    public final TableField<WorkspaceRecord, java.util.UUID> UUID = createField(DSL.name("uuid"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>workspace.created_at</code>.
     */
    public final TableField<WorkspaceRecord, LocalDateTime> CREATED_AT = createField(DSL.name("created_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>workspace.updated_at</code>.
     */
    public final TableField<WorkspaceRecord, LocalDateTime> UPDATED_AT = createField(DSL.name("updated_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>workspace.deleted_at</code>.
     */
    public final TableField<WorkspaceRecord, LocalDateTime> DELETED_AT = createField(DSL.name("deleted_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>workspace.airbyte_workspace_uuid</code>.
     */
    public final TableField<WorkspaceRecord, java.util.UUID> AIRBYTE_WORKSPACE_UUID = createField(DSL.name("airbyte_workspace_uuid"), SQLDataType.UUID, this, "");

    /**
     * The column <code>workspace.name</code>.
     */
    public final TableField<WorkspaceRecord, String> NAME = createField(DSL.name("name"), SQLDataType.VARCHAR(250).nullable(false), this, "");

    private Workspace(Name alias, Table<WorkspaceRecord> aliased) {
        this(alias, aliased, null);
    }

    private Workspace(Name alias, Table<WorkspaceRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>workspace</code> table reference
     */
    public Workspace(String alias) {
        this(DSL.name(alias), WORKSPACE);
    }

    /**
     * Create an aliased <code>workspace</code> table reference
     */
    public Workspace(Name alias) {
        this(alias, WORKSPACE);
    }

    /**
     * Create a <code>workspace</code> table reference
     */
    public Workspace() {
        this(DSL.name("workspace"), null);
    }

    public <O extends Record> Workspace(Table<O> child, ForeignKey<O, WorkspaceRecord> key) {
        super(child, key, WORKSPACE);
    }

    @Override
    @Nullable
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    @NotNull
    public Identity<WorkspaceRecord, Long> getIdentity() {
        return (Identity<WorkspaceRecord, Long>) super.getIdentity();
    }

    @Override
    @NotNull
    public UniqueKey<WorkspaceRecord> getPrimaryKey() {
        return Keys.KEY_WORKSPACE_PRIMARY;
    }

    @Override
    @NotNull
    public List<UniqueKey<WorkspaceRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.KEY_WORKSPACE_UUID);
    }

    @Override
    @NotNull
    public Workspace as(String alias) {
        return new Workspace(DSL.name(alias), this);
    }

    @Override
    @NotNull
    public Workspace as(Name alias) {
        return new Workspace(alias, this);
    }

    @Override
    @NotNull
    public Workspace as(Table<?> alias) {
        return new Workspace(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public Workspace rename(String name) {
        return new Workspace(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public Workspace rename(Name name) {
        return new Workspace(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public Workspace rename(Table<?> name) {
        return new Workspace(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row7 type methods
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Row7<Long, java.util.UUID, LocalDateTime, LocalDateTime, LocalDateTime, java.util.UUID, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function7<? super Long, ? super java.util.UUID, ? super LocalDateTime, ? super LocalDateTime, ? super LocalDateTime, ? super java.util.UUID, ? super String, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function7<? super Long, ? super java.util.UUID, ? super LocalDateTime, ? super LocalDateTime, ? super LocalDateTime, ? super java.util.UUID, ? super String, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}

