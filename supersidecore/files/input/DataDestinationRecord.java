
package com.superside.superads.domain.jooq.tables;


import java.time.LocalDateTime;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;



@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DataDestinationRecord extends UpdatableRecordImpl<DataDestinationRecord> implements Record8<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, UUID, UUID> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>data_destination.id</code>.
     */
    public DataDestinationRecord setId(@Nullable Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.id</code>.
     */
    @Nullable
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>data_destination.uuid</code>.
     */
    public DataDestinationRecord setUuid(@NotNull UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.uuid</code>.
     */
    @NotNull
    public UUID getUuid() {
        return (UUID) get(1);
    }

    /**
     * Setter for <code>data_destination.created_at</code>.
     */
    public DataDestinationRecord setCreatedAt(@Nullable LocalDateTime value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.created_at</code>.
     */
    @Nullable
    public LocalDateTime getCreatedAt() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>data_destination.updated_at</code>.
     */
    public DataDestinationRecord setUpdatedAt(@Nullable LocalDateTime value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.updated_at</code>.
     */
    @Nullable
    public LocalDateTime getUpdatedAt() {
        return (LocalDateTime) get(3);
    }

    /**
     * Setter for <code>data_destination.deleted_at</code>.
     */
    public DataDestinationRecord setDeletedAt(@Nullable LocalDateTime value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.deleted_at</code>.
     */
    @Nullable
    public LocalDateTime getDeletedAt() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>data_destination.type</code>.
     */
    public DataDestinationRecord setType(@NotNull String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.type</code>.
     */
    @NotNull
    public String getType() {
        return (String) get(5);
    }

    /**
     * Setter for <code>data_destination.workspace_uuid</code>.
     */
    public DataDestinationRecord setWorkspaceUuid(@NotNull UUID value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.workspace_uuid</code>.
     */
    @NotNull
    public UUID getWorkspaceUuid() {
        return (UUID) get(6);
    }

    /**
     * Setter for <code>data_destination.airbyte_destination_uuid</code>.
     */
    public DataDestinationRecord setAirbyteDestinationUuid(@Nullable UUID value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>data_destination.airbyte_destination_uuid</code>.
     */
    @Nullable
    public UUID getAirbyteDestinationUuid() {
        return (UUID) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Row8<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, UUID, UUID> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    @Override
    @NotNull
    public Row8<Long, UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, UUID, UUID> valuesRow() {
        return (Row8) super.valuesRow();
    }

    @Override
    @NotNull
    public Field<Long> field1() {
        return DataDestination.DATA_DESTINATION.ID;
    }

    @Override
    @NotNull
    public Field<UUID> field2() {
        return DataDestination.DATA_DESTINATION.UUID;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field3() {
        return DataDestination.DATA_DESTINATION.CREATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field4() {
        return DataDestination.DATA_DESTINATION.UPDATED_AT;
    }

    @Override
    @NotNull
    public Field<LocalDateTime> field5() {
        return DataDestination.DATA_DESTINATION.DELETED_AT;
    }

    @Override
    @NotNull
    public Field<String> field6() {
        return DataDestination.DATA_DESTINATION.TYPE;
    }

    @Override
    @NotNull
    public Field<UUID> field7() {
        return DataDestination.DATA_DESTINATION.WORKSPACE_UUID;
    }

    @Override
    @NotNull
    public Field<UUID> field8() {
        return DataDestination.DATA_DESTINATION.AIRBYTE_DESTINATION_UUID;
    }

    @Override
    @Nullable
    public Long component1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID component2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime component3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime component5() {
        return getDeletedAt();
    }

    @Override
    @NotNull
    public String component6() {
        return getType();
    }

    @Override
    @NotNull
    public UUID component7() {
        return getWorkspaceUuid();
    }

    @Override
    @Nullable
    public UUID component8() {
        return getAirbyteDestinationUuid();
    }

    @Override
    @Nullable
    public Long value1() {
        return getId();
    }

    @Override
    @NotNull
    public UUID value2() {
        return getUuid();
    }

    @Override
    @Nullable
    public LocalDateTime value3() {
        return getCreatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value4() {
        return getUpdatedAt();
    }

    @Override
    @Nullable
    public LocalDateTime value5() {
        return getDeletedAt();
    }

    @Override
    @NotNull
    public String value6() {
        return getType();
    }

    @Override
    @NotNull
    public UUID value7() {
        return getWorkspaceUuid();
    }

    @Override
    @Nullable
    public UUID value8() {
        return getAirbyteDestinationUuid();
    }

    @Override
    @NotNull
    public DataDestinationRecord value1(@Nullable Long value) {
        setId(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value2(@NotNull UUID value) {
        setUuid(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value3(@Nullable LocalDateTime value) {
        setCreatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value4(@Nullable LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value5(@Nullable LocalDateTime value) {
        setDeletedAt(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value6(@NotNull String value) {
        setType(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value7(@NotNull UUID value) {
        setWorkspaceUuid(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord value8(@Nullable UUID value) {
        setAirbyteDestinationUuid(value);
        return this;
    }

    @Override
    @NotNull
    public DataDestinationRecord values(@Nullable Long value1, @NotNull UUID value2, @Nullable LocalDateTime value3, @Nullable LocalDateTime value4, @Nullable LocalDateTime value5, @NotNull String value6, @NotNull UUID value7, @Nullable UUID value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached DataDestinationRecord
     */
    public DataDestinationRecord() {
        super(DataDestination.DATA_DESTINATION);
    }

    /**
     * Create a detached, initialised DataDestinationRecord
     */
    public DataDestinationRecord(@Nullable Long id, @NotNull UUID uuid, @Nullable LocalDateTime createdAt, @Nullable LocalDateTime updatedAt, @Nullable LocalDateTime deletedAt, @NotNull String type, @NotNull UUID workspaceUuid, @Nullable UUID airbyteDestinationUuid) {
        super(DataDestination.DATA_DESTINATION);

        setId(id);
        setUuid(uuid);
        setCreatedAt(createdAt);
        setUpdatedAt(updatedAt);
        setDeletedAt(deletedAt);
        setType(type);
        setWorkspaceUuid(workspaceUuid);
        setAirbyteDestinationUuid(airbyteDestinationUuid);
        resetChangedOnNotNull();
    }

    /**
     * Create a detached, initialised DataDestinationRecord
     */
    public DataDestinationRecord(com.superside.superads.domain.jooq.tables.pojos.DataDestination value) {
        super(DataDestination.DATA_DESTINATION);

        if (value != null) {
            setId(value.getId());
            setUuid(value.getUuid());
            setCreatedAt(value.getCreatedAt());
            setUpdatedAt(value.getUpdatedAt());
            setDeletedAt(value.getDeletedAt());
            setType(value.getType());
            setWorkspaceUuid(value.getWorkspaceUuid());
            setAirbyteDestinationUuid(value.getAirbyteDestinationUuid());
            resetChangedOnNotNull();
        }
    }
}
