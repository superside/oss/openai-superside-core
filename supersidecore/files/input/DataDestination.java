
package com.superside.superads.domain.jooq.tables;


import com.superside.superads.domain.jooq.DefaultSchema;
import com.superside.superads.domain.jooq.Keys;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function8;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;



@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DataDestination extends TableImpl<DataDestinationRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>data_destination</code>
     */
    public static final DataDestination DATA_DESTINATION = new DataDestination();

    /**
     * The class holding records for this type
     */
    @Override
    @NotNull
    public Class<DataDestinationRecord> getRecordType() {
        return DataDestinationRecord.class;
    }

    /**
     * The column <code>data_destination.id</code>.
     */
    public final TableField<DataDestinationRecord, Long> ID = createField(DSL.name("id"), SQLDataType.BIGINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>data_destination.uuid</code>.
     */
    public final TableField<DataDestinationRecord, java.util.UUID> UUID = createField(DSL.name("uuid"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>data_destination.created_at</code>.
     */
    public final TableField<DataDestinationRecord, LocalDateTime> CREATED_AT = createField(DSL.name("created_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>data_destination.updated_at</code>.
     */
    public final TableField<DataDestinationRecord, LocalDateTime> UPDATED_AT = createField(DSL.name("updated_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>data_destination.deleted_at</code>.
     */
    public final TableField<DataDestinationRecord, LocalDateTime> DELETED_AT = createField(DSL.name("deleted_at"), SQLDataType.LOCALDATETIME(0), this, "");

    /**
     * The column <code>data_destination.type</code>.
     */
    public final TableField<DataDestinationRecord, String> TYPE = createField(DSL.name("type"), SQLDataType.VARCHAR(250).nullable(false), this, "");

    /**
     * The column <code>data_destination.workspace_uuid</code>.
     */
    public final TableField<DataDestinationRecord, java.util.UUID> WORKSPACE_UUID = createField(DSL.name("workspace_uuid"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>data_destination.airbyte_destination_uuid</code>.
     */
    public final TableField<DataDestinationRecord, java.util.UUID> AIRBYTE_DESTINATION_UUID = createField(DSL.name("airbyte_destination_uuid"), SQLDataType.UUID, this, "");

    private DataDestination(Name alias, Table<DataDestinationRecord> aliased) {
        this(alias, aliased, null);
    }

    private DataDestination(Name alias, Table<DataDestinationRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>data_destination</code> table reference
     */
    public DataDestination(String alias) {
        this(DSL.name(alias), DATA_DESTINATION);
    }

    /**
     * Create an aliased <code>data_destination</code> table reference
     */
    public DataDestination(Name alias) {
        this(alias, DATA_DESTINATION);
    }

    /**
     * Create a <code>data_destination</code> table reference
     */
    public DataDestination() {
        this(DSL.name("data_destination"), null);
    }

    public <O extends Record> DataDestination(Table<O> child, ForeignKey<O, DataDestinationRecord> key) {
        super(child, key, DATA_DESTINATION);
    }

    @Override
    @Nullable
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    @NotNull
    public Identity<DataDestinationRecord, Long> getIdentity() {
        return (Identity<DataDestinationRecord, Long>) super.getIdentity();
    }

    @Override
    @NotNull
    public UniqueKey<DataDestinationRecord> getPrimaryKey() {
        return Keys.KEY_DATA_DESTINATION_PRIMARY;
    }

    @Override
    @NotNull
    public List<UniqueKey<DataDestinationRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.KEY_DATA_DESTINATION_UUID);
    }

    @Override
    @NotNull
    public DataDestination as(String alias) {
        return new DataDestination(DSL.name(alias), this);
    }

    @Override
    @NotNull
    public DataDestination as(Name alias) {
        return new DataDestination(alias, this);
    }

    @Override
    @NotNull
    public DataDestination as(Table<?> alias) {
        return new DataDestination(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public DataDestination rename(String name) {
        return new DataDestination(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public DataDestination rename(Name name) {
        return new DataDestination(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    @NotNull
    public DataDestination rename(Table<?> name) {
        return new DataDestination(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    @NotNull
    public Row8<Long, java.util.UUID, LocalDateTime, LocalDateTime, LocalDateTime, String, java.util.UUID, java.util.UUID> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function8<? super Long, ? super java.util.UUID, ? super LocalDateTime, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? super java.util.UUID, ? super java.util.UUID, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function8<? super Long, ? super java.util.UUID, ? super LocalDateTime, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? super java.util.UUID, ? super java.util.UUID, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
