import os
import re
import json
import openai


class ClassType:
    MODEL = "model"
    REPOSITORY = "repository"


def remove_text(text):
    # Remove the text block using the sub() function from the re module
    text = re.sub('@Override\s*public\s+boolean\s+equals\(Object obj\) \{.*?\}', '', text, flags=re.DOTALL)
    # remove functions
    p = r"((@Override\s+@NotNull\s|@Override\s+@Nullable\s)(public\s+[a-zA-Z<>\[\],?\. ]+\s+(field|value|component)\d+[\s\S]+?\{[\s\S]+?return\s+\S+;\s+\}))"
    text = re.sub(p, '', text, flags=re.DOTALL)
    # remove comments
    text = re.sub(r'/\*.*?\*/', '', text, flags=re.DOTALL)
    # Remove the constructor
    return text


def trim(text):
    text = ' '.join(text.split())
    text = re.sub(r"//.*", "", text)
    return remove_text(text)


def trim_whitespace(text):
    text = ' '.join(text.split())
    text = re.sub(r"//.*", "", text)
    return re.sub(r'/\*.*?\*/', '', text, flags=re.DOTALL)


def createExample(recordFile, classType, className, out):
    return f"""
    Example:
    -   Recordfile:
        ```{recordFile}```
        
        Output {className}.kt
        {out}
    """


def create_prompt(recordFile, className):
    return f"""

Generate the content for the following data:
Recordfile:
```{recordFile}```
        
Only generate the content for the file, no description, prefixes or quotes is needed. Generate the file that contains class {className}
"""


def camel_case(test_str):
    return test_str[0].lower() + test_str[1:]


def pascal_case(test_str):
    return test_str[0].upper() + test_str[1:]


def find_file_with_content(start_dir, content):
    for root, dirs, files in os.walk(start_dir):
        for file in files:
            file_path = os.path.join(root, file)
            with open(file_path, 'r') as f:
                file_content = f.read()
                if content in file_content:
                    return trim(file_content)
    return None


def find_model_classes(start_dir):
    classes = []
    for root, dirs, files in os.walk(start_dir):
        for file in files:
            file_path = os.path.join(root, file)
            pattern = r"data\s+class\s+(\w+)\s*\([^)]*\)\s*:\s*BaseModel<\1>"
            pattern_compiled = re.compile(pattern, re.DOTALL)
            with open(file_path, 'r', encoding="utf-8", errors="ignore") as f:
                content = f.read()
                matches = pattern_compiled.findall(content)

            c = None
            package = None

            for match in matches:
                c = match.strip()  # Trim leading/trailing whitespace
            pattern = r"package\s+(\S+)\n"
            pattern_compiled = re.compile(pattern)

            match = pattern_compiled.search(content)
            if match:
                package = match.group(1)

            if (c is not None):
                classes.append(package + "." + c)

    return classes


def get_entity_from_class_name(class_name):
    if class_name.endswith("Repository"):
        string = class_name[:-len("Repository")]
    elif class_name.endswith("Model"):
        string = class_name[:-len("Model")]
    else:
        string = class_name
    return string.strip()


def read_config(current_directory):
    files = os.listdir(current_directory)
    if "supersidecore.json" in files:
        print("Config file found!")
        # Open "supersidecore.json" and read the contents
        with open("supersidecore.json") as f:
            data = json.load(f)
            return data

    else:
        raise Exception("Config File not found.")


def get_classtype_from_class_name(simpleClassName):
    if ("Repository" in simpleClassName):
        return ClassType.REPOSITORY
    elif "Model" in simpleClassName:
        return ClassType.MODEL
    else:
        return ClassType.MODEL


def find_folder_with_subpath(start_dir, subpath):
    for root, dirs, files in os.walk(start_dir):
        if subpath in root:
            return root
    return None


def get_path_from_canonical_name(name):
    parts = name.split(".")
    ret_string = ".".join(parts[:-1])
    ret_string = ret_string.replace(".", "/")
    return ret_string

def try_removing_description(content):
    match = re.search(r'```kotlin\n([\s\S]+?)\n```', content)

    if match:
        content_to_keep = match.group(1)
        return content_to_keep
    return content

# generate base case
modelText = "Take Recordfile, extract the fields and generate the corresponding repository\n\n"
repositoryText = "Take the recordfile, extract the fields and generate the corresponding repository\n\n"
text = {
    ClassType.MODEL: "",
    ClassType.REPOSITORY: ""

}

from importlib.resources import files

for classType in [ClassType.MODEL, ClassType.REPOSITORY]:
    for item in ["AdsAccount", "Workspace"]:
        recordFile = trim(open(files("supersidecore").joinpath(f'files/input/{item}Record.java'), 'r').read())
        outputFile = open(files("supersidecore").joinpath(f'files/output/{item}{pascal_case(classType)}.kt'),
                          'r').read()
        text[classType] += createExample(recordFile, classType,
                                         f"com.superads.api.{camel_case(item)}.{item}{pascal_case(classType)}",
                                         outputFile)


def main():
    # Check if "supersidecore.json" exists in the list of files
    current_directory = os.getcwd()
    config = read_config(current_directory)
    openaiApiKey = config["openaiApiKey"]
    jooqFolder = config["jooqFolder"]
    model = config["gptModel"] if "gptModel" in config else "gpt-3.5-turbo"

    canonicalName = input("Enter model canonical name (e.g.  com.superads.api.dataDestination.DataDestinationModel):")
    simpleClassName = canonicalName.split(".")[-1]
    classType = get_classtype_from_class_name(simpleClassName)
    entityName = get_entity_from_class_name(simpleClassName)

    # find recordFile
    jooqAbsolutePath = os.path.join(current_directory, jooqFolder)
    recordFile = find_file_with_content(jooqAbsolutePath,
                                        f"class {entityName}Record extends UpdatableRecordImpl<{entityName}Record>")

    shouldRenameModel = False
    if classType == ClassType.MODEL and not simpleClassName.endswith("Model"):
        shouldRenameModel = True
        canonicalName += "Model"

    prompt = text[classType] + create_prompt(recordFile, canonicalName)

    prompt += "The model classes in the code base is:\n"
    prompt += "".join(
        map(lambda m: "  -" + m + "\n", find_model_classes(current_directory))
    )
    prompt +="When you have a field in the model class that matches a model class, don't use UUID but rather Id class (coming from com.superside.core.model.Id)."

    print(prompt)

    # get the prompt
    openai.api_key = openaiApiKey
    print("generating content...")
    x = openai.ChatCompletion.create(
        model=model,
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": prompt},
        ]
    )

    data = try_removing_description(x["choices"][0]["message"]["content"])

    if shouldRenameModel:
        data = data.replace(simpleClassName + "Model", entityName)
        print("should rename model")

    # create file
    path = get_path_from_canonical_name(canonicalName)
    folder = find_folder_with_subpath(current_directory, path)
    if (folder == None):
        folder = find_folder_with_subpath(current_directory, "src/main/kotlin/" + "/".join(path.split("/")[0:3]))
        folder = os.path.join(folder, "/".join(path.split("/")[3:]))
        print(os.makedirs(folder, exist_ok=True))
        print("subpath not found, creating...")

    file_path = os.path.join(folder, simpleClassName + ".kt")
    with open(file_path, 'w') as f:
        f.write(data)

    print(f"generating file {path}/{simpleClassName}.kt")


if __name__ == "__main__":
    main()
