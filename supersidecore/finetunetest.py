import re

trainingData = []


class ClassType:
    MODEL = "model"
    REPOSITORY = "repository"



def remove_text(text, simpleClass):

    # Remove the text block using the sub() function from the re module
    text = re.sub('@Override\s*public\s+boolean\s+equals\(Object obj\) \{.*?\}', '', text, flags=re.DOTALL)
    #remove functions
    p = r"((@Override\s+@NotNull\s|@Override\s+@Nullable\s)(public\s+[a-zA-Z<>\[\],?\. ]+\s+(field|value|component)\d+[\s\S]+?\{[\s\S]+?return\s+\S+;\s+\}))"
    text = re.sub(p, '', text, flags=re.DOTALL)
    #remove comments
    text= re.sub( r'/\*.*?\*/', '', text, flags=re.DOTALL)
    return text


def trim(text, simpleClass):
    text = ' '.join(text.split())
    text =  re.sub(r"//.*", "", text)
    return remove_text(text, simpleClass)

def trim_whitespace(text):
    text = ' '.join(text.split())
    text =  re.sub(r"//.*", "", text)
    return re.sub( r'/\*.*?\*/', '', text, flags=re.DOTALL)

def createPrompt( recordFile, classType, className, out):
    return f"""
    Example:
    -   Recordfile:
        ```{recordFile}```
        
        Generated class name: 
        {className}
        
        Generated class type:
        {classType}
        
        Output content:
        {out} 
    """


def createCompletion(classType, className, content):
    return " "+content


def camelCase(test_str):
    return test_str[0].lower() + test_str[1:]


# generate base case



recordFile = trim(open(f'../files/input/DataDestinationRecord.java', 'r').read(), "DataDestination")
className = input("Enter class name: ")
classType = input("Enter class type: ")


text = f"""Generate the content for the following data:
Recordfile:
```{recordFile}```
        
Generated class name: 
{className}
        
Generated class type:
{classType}
"""

import openai

print(text)

x = openai.Completion.create(
    model="davinci:ft-superside-2023-05-26-10-23-19",
    prompt=text,
    max_tokens=1300

)

print(x)