trainingData = []


class ClassType:
    MODEL = "model"
    REPOSITORY = "repository"

import re



def remove_text(text, simpleClass):

    # Remove the text block using the sub() function from the re module
    text = re.sub('@Override\s*public\s+boolean\s+equals\(Object obj\) \{.*?\}', '', text, flags=re.DOTALL)
    #remove functions
    p = r"((@Override\s+@NotNull\s|@Override\s+@Nullable\s)(public\s+[a-zA-Z<>\[\],?\. ]+\s+(field|value|component)\d+[\s\S]+?\{[\s\S]+?return\s+\S+;\s+\}))"
    text = re.sub(p, '', text, flags=re.DOTALL)
    #remove comments
    text= re.sub( r'/\*.*?\*/', '', text, flags=re.DOTALL)
    # Remove the constructor
    text = re.sub(f'public\s+{simpleClass}\(.+?}}', '', text, flags=re.DOTALL)
    return text


def trim(text, simpleClass):
    text = ' '.join(text.split())
    text =  re.sub(r"//.*", "", text)
    return remove_text(text, simpleClass)

def trim_whitespace(text):
    text = ' '.join(text.split())
    text =  re.sub(r"//.*", "", text)
    return re.sub( r'/\*.*?\*/', '', text, flags=re.DOTALL)

def createPrompt(tableFile, recordFile, classType, className):
    return f"""   
    Recordfile
    ```java
    {recordFile}
    ```
    Generated class type: {classType}
    Generated class name: {className}
    
    What is the content of the generated kotlin file using Superside core?
    """


def createCompletion(classType, className, content):
    return " "+content


def camelCase(test_str):
    return test_str[0].lower() + test_str[1:]


# generate base case
for item in ["AdsAccount", "DataDestination", "Workspace"]:
    tableFile = trim(open(f'files/input/{item}.java', 'r').read(), item)
    recordFile = trim(open(f'files/input/{item}Record.java', 'r').read(), item)
    modelFile = open(f'files/output/{item}Model.kt', 'r').read()
    repositoryFile = open(f'files/output/{item}Repository.kt', 'r').read()
    trainingData.append(
        {
            "prompt": createPrompt(tableFile, recordFile, ClassType.MODEL,
                                   f"com.superads.api.{camelCase(item)}.{item}Model"),
            "completion": createCompletion(ClassType.MODEL, f"com.superads.api.{camelCase(item)}.{item}Model",
                                           modelFile)
        }
    )
    trainingData.append(
        {
            "prompt": createPrompt(tableFile, recordFile, ClassType.REPOSITORY,
                                   f"com.superads.api.{camelCase(item)}.{item}Repository"),
            "completion": createCompletion(ClassType.REPOSITORY, f"com.superads.api.{camelCase(item)}.{item}Repository",
                                           repositoryFile)
        }
    )

# generate different variations
for path in ["com.superside.superspace.domain", "com.superside.designcollab.jooq", "com.superside.data.jooqgenerated"]:
    for path1 in ["com.superside.superspace", "com.superside.subscription", "com.superside.assets"]:
        trainingData.append({
            "prompt": createPrompt(
                tableFile.replace("com.superside.superads.domain.jooq", path),
                recordFile.replace("com.superside.superads.domain.jooq", path),
                ClassType.MODEL,
                f"{path1}.{camelCase(item)}.{item}Model"
            ),
            "completion": createCompletion(ClassType.MODEL, f"{path1}.{camelCase(item)}.{item}Model",
                                           modelFile.replace("com.superside.superads.domain.jooq", path).replace(
                                               f"com.superads.api.${camelCase(item)}", f"{path1}.{camelCase(item)}")
                                           )
        })
        trainingData.append({
            "prompt": createPrompt(
                tableFile.replace("com.superside.superads.domain.jooq", path),
                recordFile.replace("com.superside.superads.domain.jooq", path),
                ClassType.REPOSITORY,
                f"{path1}.{camelCase(item)}.{item}Repository"
            ),
            "completion": createCompletion(ClassType.REPOSITORY, f"{path1}.{camelCase(item)}.{item}Repository",
                                           modelFile.replace("com.superside.superads.domain.jooq", path).replace(
                                               f"com.superads.api.${camelCase(item)}", f"{path1}.{camelCase(item)}")
                                           )
        })
        trainingData.append({
            "prompt": createPrompt(
                tableFile.replace("com.superside.superads.domain.jooq", path),
                recordFile.replace("com.superside.superads.domain.jooq", path),
                ClassType.MODEL,
                f"{path1}.model.{item}Model"
            ),
            "completion": createCompletion(
                ClassType.MODEL, f"{path1}.{camelCase(item)}.{item}Model",
                modelFile.replace("com.superside.superads.domain.jooq", path).replace(
                    f"com.superads.api.${camelCase(item)}", f"{path1}.model.{item}Model")
            )
        })
        trainingData.append({
            "prompt": createPrompt(
                tableFile.replace("com.superside.superads.domain.jooq", path),
                recordFile.replace("com.superside.superads.domain.jooq", path),
                ClassType.REPOSITORY,
                f"{path1}.model.{item}Repository"
            ),
            "completion": createCompletion(
                ClassType.REPOSITORY, f"{path1}.model.{item}Repository",
                modelFile.replace("com.superside.superads.domain.jooq", path).replace(
                    f"com.superads.api.${camelCase(item)}", f"{path1}.model.${item}Repository")
            )
        })


import json
import openai

file = "finetunecqrs.jsonl"
with open(file, "w") as output_file:
    for entry in trainingData:
        json.dump(entry, output_file)
        output_file.write("\n")


upload_response = openai.File.create(
    file=open(file, "rb"),
    purpose='fine-tune'
)
file_id = upload_response.id
upload_response

print(upload_response)

fine_tune_response = openai.FineTune.create(training_file=file_id, model="davinci")
print(fine_tune_response)

