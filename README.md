# How to install

1. Install python3 and pip3 (if not installed)
2. python3 setup.py sdist bdist_wheel
3. pip install dist/openai-supersidecore-0.0.1.tar.gz


# How to use
1. Create a file named supersideconfig.json in the project root directory with the following properties
```json
{
  "openaiApiKey": "OPENAIAPIKEY",
  "jooqFolder": "relative path to the jooq module"
}
```
2. type superside-core-generate in the command line with the project root as the directory