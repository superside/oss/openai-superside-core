from setuptools import setup, find_packages
import glob

input_files = []
output_files = []

# Find all text files recursively within the 'data' folder and its subfolders
text_files = glob.glob(pathname='supersidecore/files/input/*.', recursive=True)
if text_files:
    # Add the text files to data_files
    input_files.extend(text_files)
text_files = glob.glob(pathname='supersidecore/files/output/*', recursive=True)
if text_files:
    # Add the text files to data_files
    output_files.extend(text_files)

setup(
    name="openai-supersidecore",
    version='0.0.1',
    author='Jing Venaas Kjeldsen',
    author_email='jing@superside.com',
    package_data={'supersidecore': ['files/input/*', 'files/output/*']},
    description='Superside Core <3 OpenAI: Speed up manual processes through AI',
    long_description="TBD",
    long_description_content_type='text/markdown',
    url='https://gitlab.com/superside/oss/openai-superside-core',
    packages=find_packages(),
    install_requires=[
        'openai',

    ],
    entry_points={
        'console_scripts': [
            'superside-core-generate = supersidecore.core:main',
        ],
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)